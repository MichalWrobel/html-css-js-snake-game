Classic Snake Game in Javascript made for fun to play with svg's and canvas. It could have better performance and could be responsive but I'm not feeling like I'm want to pick up this old project. It was more for js than for styling so please keep it in mind. Game is working properly on newest Chrome, please don't be suprised when your browser other than V8 based can't fire it.


PLAY IT HERE: https://michalwrobel.bitbucket.io/snake/

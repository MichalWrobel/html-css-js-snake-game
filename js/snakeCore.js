'use strict';

window.onload = function() {

	let started = false;

	let imagesSource = ['images/grassBlock.svg'	,	
						'images/wholesnake.svg'	,
						];
	let	images = [];
	let count = imagesSource.length;
	let whichFood = true;
	let canvas = document.createElement('canvas');
	canvas.id = 'mainCanvas';
	let secondCanvas = document.createElement('canvas');
	document.body.appendChild(secondCanvas);
	document.body.appendChild(canvas);

	let ctx = canvas.getContext('2d');
	const context = secondCanvas.getContext('2d');
	document.getElementById('pauseOverlay').style.display = 'none';

	//CANVAS SETTINGS
	canvas.width = 1000;
	secondCanvas.width = canvas.width;
	canvas.height = 550;
	secondCanvas.height = canvas.height;	

	canvas.style.border = '2px solid black';
	canvas.style.zIndex = '-2';

	canvas.innerHTML = 'Canvas not supported in your browser';
	secondCanvas.innerHTML = 'Canvas not supported in your browser';

	secondCanvas.style.position = 'fixed';
	secondCanvas.style.marginLeft = '2px';
	secondCanvas.style.marginTop = '2px';
	secondCanvas.style.zIndex = '-1';	

	const mainOverlay = document.getElementById('overscreen');
	const optionsItems = document.getElementsByClassName('optionItems');
	const pauseInfo = document.getElementById('pauseOverlay');		
	const clicked = document.getElementById('przyciskPlay');
	const tryAgain = document.getElementById('tryagain');
	const pausePlay = document.getElementById('przyciskPlayPause');
	const optionsBtn = document.getElementsByClassName('przyciskOptions');
	const optionsOverlay = document.getElementById('optionsOverlay');
	const gameOverScreen = document.getElementById('GO');
	const scoreBoard = document.getElementById('score');
	
	//SETTING COORDS VARIABLES
	let oldCoords = [];	

	//GAME INTERVAL
	let initialStep = 100;
	let snakeStep = initialStep;
	//INITIAL DIRECTION OF SNAKE
	let dir = 'down';
	let oldDir = 'down';
	//SNAKE PROPERTIES
	let snakeInitialLength = 15;
	let snakeLength = snakeInitialLength;
	const cellSize = 25;
	//FOOD PROPERTIES
	let fx, fy;
	let randX = 0;
	let randY = 0;

	let score = 0;
	let bestScore = 0;
	let scoreContainer = [];
	let paused = false;
	let collision = false;

	let rockSize = 100;
	let rocks = [];
	let rockCount = 4;

	let controls = {
		37: 'left',
		40: 'down',
		39: 'right',
		38: 'up'
	};
	let oppControls = {
		'left': 'right',
		'down': 'up',
		'right': 'left',
		'up': 'down'
	};
	function start() {
		started = true;
		scoreContainer.pop();
		clearInterval(snakeStep);
		snakeStep = setInterval( draw, initialStep);
		setTimeout( () => {
			setRockCoords()
			setFoodCoords();
			
		}, 10);
		setTimeout(drawRocks, 10);
		setTimeout( () => {
		if(started){
			document.getElementById('przyciskPlay').innerHTML = 'Loading...';
			ready();
		} 
	}), 1000} // TIME FOR CALCULATE FOOD AND ROCK COORDS AND TO RENDER

	optionsOverlay.style.display = 'none';


	pausePlay.onclick = function() {
		pauseHandler();
	}

	clicked.onclick =  function () {
		
		start();
		if (paused) pauseHandler();
	}
	tryagain.onclick = function () {
		tryAgain.innerHTML = 'Loading...';
		setTimeout(start, 1);
		mainOverlay.style.display = 'none';
	};

	optionsBtn[0].onclick = function() {
		optionsOverlay.style.display = 'block';
		optionsItems[4].onclick = () => optionsOverlay.style.display = 'none';

	}
	optionsBtn[1].onclick = function() {
		optionsOverlay.style.display = 'block';
		optionsItems[4].onclick = () => optionsOverlay.style.display = 'none';
	}

	function ready(){
		for(let i = 0; i < count; i++) {
		    let img = new Image();
		    images.push(img);
		    img.onload = onloadHandler;
		    img.src = imagesSource[i];
		    if (img.complete) onloadHandler().bind(img);
		}
	}
	optionsItems[0].onclick = () => {
		snakeInitialLength < 15 ? snakeInitialLength += 5 : snakeInitialLength = 5;
		optionsItems[0].innerHTML = `Snake Length: ${snakeInitialLength}`;

		if (snakeInitialLength < oldCoords.length) {
			const diff = oldCoords.length - snakeInitialLength;
			for(let i = 0; i < diff; i++) oldCoords.pop();
		}

		if (snakeInitialLength > oldCoords.length) {
			const diffe = snakeInitialLength - oldCoords.length;
			// NUMBER 500 DOESN'T MATTER HERE IT JUST FOR NOT SEEING THE SNAKE FROM PREVIOUS GAME
			for(let i = 500; i < 500 + diffe; i++) oldCoords.push({x: 0, y: i});
		}
		snakeLength = snakeInitialLength;
	};
	optionsItems[1].onclick = () => {
		initialStep < 260 ? initialStep += 25 : initialStep = 25;
		optionsItems[1].innerHTML = `Interval: ${initialStep}`;
	};
	optionsItems[2].onclick = () => {
		rockCount < 36 ? rockCount += 2 : rockCount = 0;
		optionsItems[2].innerHTML = `Rocks: ${rockCount}`;
	};
	optionsItems[3].onclick = () => {
		whichFood = !whichFood;
		optionsItems[3].innerHTML = `Snake Eats: ${whichFood ? 'Fruit' : 'Mouse'}`;
	}

	function updateSettings() {
		if (paused) {
			pauseInfo.style.display = 'none';
			paused = false;
		}

		optionsItems[0].innerHTML = `Snake Length: ${snakeInitialLength}`;
		optionsItems[1].innerHTML = `Interval: ${initialStep}`;
		optionsItems[2].innerHTML = `Rocks: ${rockCount}`;
		optionsItems[3].innerHTML = 'Snake Eats: Fruit';

	}
	function onloadHandler() {
   	 count--;
    	if (count === 0) startGame();
	}
	function render(){
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		fillCell(fx, fy, whichFood);

		for(let i = 0; i < oldCoords.length; i++){
            let segment = oldCoords[i];
            let segx = segment.x;
            let segy = segment.y;
            let tilex = segx*cellSize;	
            let tiley = segy*cellSize;
            let tx = 4;
            let ty = 4;

		if (i === 0) {
			let nseg = oldCoords[i+1];
			if(segy < nseg.y) {
				tx = 2; ty = 2;
			} else if(segx > nseg.x) {
				tx = 1; ty = 2;
			} else if(segy > nseg.y) {
				tx = 2; ty = 3;
			} else if(segx < nseg.x) {
				tx = 1; ty = 3;
			}
		} else if(i === oldCoords.length - 1) {
			let pseg = oldCoords[i-1];
			if(pseg.y < segy) {
				tx = 2; ty = 1;
			} else if(pseg.x > segx) {
				tx = 3; ty = 2;
			} else if(pseg.y > segy) {
				tx = 3; ty = 0;
			} else if(pseg.x < segx) {
				tx = 3; ty = 1;
			}
		} else {
			let pseg = oldCoords[i-1];
			let nseg = oldCoords[i+1];
			if(pseg.x < segx && nseg.x > segx || nseg.x < segx && pseg.x > segx) {
				tx = 1; ty = 0;
			} else if(pseg.x < segx && nseg.y > segy || nseg.x < segx && pseg.y > segy) {
				tx = 2; ty = 0;
			} else if(pseg.y < segy && nseg.y > segy || nseg.y < segy && pseg.y > segy) {
				tx = 0; ty = 1;
			} else if(pseg.y < segy && nseg.x < segx || nseg.y < segy && pseg.x < segx) {
				tx = 1; ty = 1;
			} else if(pseg.x > segx && nseg.y < segy || nseg.x > segx && pseg.y < segy) {
				tx = 0; ty = 2;
			} else if(pseg.y > segy && nseg.x > segx || nseg.y > segy && pseg.x > segx) {
				tx = 0; ty = 0;
			}
		}
		if (oldCoords[0].x === -1 || 
			oldCoords[0].x === canvas.width/cellSize + 1 ||
			(oldCoords[0].y === -1 && dir !== 'down' && oldDir !== 'down')||
			oldCoords[0].y === canvas.height/cellSize  ) {
			collision = true;
		}
		// USED TO HAVE NO BOUNDARIES BUT IT REQUIRES ADDITIONAL WORK WITH SVG AND I DON'T CARE ABOUT THIS PROJECT THIS MUCH :)
		// if (oldCoords[0].x === -1) {oldCoords[0].x = canvas.width / cellSize ;}
		// else if (oldCoords[0].x === canvas.width / cellSize ) {oldCoords[0].x = 0;} 
		// 	 if (oldCoords[0].y === -1) {oldCoords[0].y = canvas.height / cellSize ;	}
		// else if (oldCoords[0].y === canvas.height / cellSize) {oldCoords[0].y = 0;}

			ctx.drawImage(images[1], tx*50, ty*50, 50, 50, tilex, tiley, cellSize, cellSize);
		}
	}
	function pauseHandler() {
		if (mainOverlay.style.display === 'block' || optionsOverlay.style.display === 'block') {

			return;
		}
		if (paused) {
			clearInterval(snakeStep);
			snakeStep = setInterval( draw, initialStep);
			paused = false;
			pauseInfo.style.display = 'none';
		} else {
			clearInterval(snakeStep);
			paused = true;
			pauseInfo.style.display = 'block';
		}
	}
	function snake(){
		// snakeLength = snakeInitialLength;
		for(let i = snakeLength - 1; i >= 0; i--){
			oldCoords.push({x: 0, y: i});
		}
	}

	window.onkeydown = function(event){
		dir = controls[event.keyCode];
		if (!paused && tryAgain.style.display !== 'block') {
			if(dir === 'left'  && oldDir === 'right'  ||
			dir === 'right' 	&& oldDir === 'left'   || 
			dir === 'down' 	 	 && oldDir === 'up'	 || 
			dir === 'up' 		  && oldDir === 'down') dir = oppControls[dir];
			if(dir != undefined) oldDir = dir;
			if(!started) {
				start();
				dir = 'down';
			}
		}
		overscreen.style.display = 'none';
		event.preventDefault();

		if (started && event.keyCode === 80) {
			pauseHandler();
		}
	}
	function setRockCoords(){
		for(let i = 0; i < rockCount; i++){
			rocks.push({x: createFoodX(), y: createFoodY()});		
		}
	}
	function startGame(){
		setTimeout( () => { 
			snake();
			setTimeout(drawRocks, 10);
			//setRockCoords();
			// setFoodCoords();
			document.getElementById('mainscreen').style.display = 'none';
		}, 10);
	}

	function newRockCoords(nr){
		rocks[nr].x = createFoodX();
		rocks[nr].y = createFoodY();
		if(rockRequirement(nr)){
			context.drawImage(images[0], rocks[nr].x, rocks[nr].y, rockSize, rockSize);
		} else newRockCoords(nr);
	}
	function setFoodCoords(){
		fx = createFoodX();
		fy = createFoodY();
	}
	function createFoodX(){
		do {
			randX = Math.trunc(Math.random()*100)*cellSize;
		} while(randX > canvas.width - cellSize);

		return randX;
	}
	function drawRocks(){
		for(let i = 0; i < rockCount; i++){
			if(rockRequirement(i)){
				context.drawImage(images[0], rocks[i].x, rocks[i].y, rockSize, rockSize);
			} else newRockCoords(i);
		}

	}
	function rockRequirement(rock){
		if(rocks[rock].x < canvas.width-rockSize && rocks[rock].y < canvas.height-rockSize && 
			fx != rocks[rock].x && fy!= rocks[rock].y && rocks[rock].x != 0) return true;

		return false;
	}
	function createFoodY(){
		do {
		randY = Math.trunc(Math.random()*100)*cellSize;
		} while(randY > canvas.height - cellSize);
		return randY;
	}
	function snakeEat(){
		score++;
		drawScore();
		setFoodCoords();
		oldCoords.push({x: oldCoords[snakeLength-1].x, y: oldCoords[snakeLength-1].y});
		snakeLength++;
	}
	function drawScore(){
		scoreBoard.innerHTML = "Score: " + "<font color='#4ce600'>" + score + "</font>" + ", " + " Best Score: "  +"<font color='#4ce600'>" +bestScore + "</font>, PRESS <font color='#4ce600'>[P]</font> TO PAUSE GAME";
	}
	function gameOver(){
		collision = false;
		scoreContainer.push(score);
		mainOverlay.style.display = 'block';
		tryAgain.innerHTML = 'TRY AGAIN';
		gameOverScreen.innerHTML = "<h1>GAME <font color='#4ce600'>OVER</font></h1>Your score: <font color='#4ce600'><h2 id = 'scoreOutput'>" + scoreContainer[0] + "</h2></font>";

		if(score > bestScore) bestScore = score;
		score = 0;
		drawScore();
		started = false;
		
		for(let i = snakeInitialLength; i < snakeLength; i++){
			oldCoords.pop();
		}

		context.clearRect(0, 0, canvas.width, canvas.height);

		rocks = [];

		snakeLength = snakeInitialLength;

		for(let i = 0; i < snakeLength; i++){
			oldCoords[i].x = 0;
			oldCoords[i].y = -1;
		}

		dir = null;
		oldDir = 'down';
	}
	function setDirection(direction){
		switch(direction){
			case 'right' : oldCoords[0].x++; break;
			case 'left'  : oldCoords[0].x--; break;
			case 'down'  : oldCoords[0].y++; break;
			case 'up'	 : oldCoords[0].y--; break;
			default 	 : setDirection(oldDir); break; 
		}
	}
	function draw(){
		if (oldCoords.length) {

			if(started){
			render();
			if(oldCoords[0].x*cellSize === fx && oldCoords[0].y*cellSize === fy) snakeEat();

				for(let i = 0; i < rockCount; i++){
					for(let k = 0; k < rockSize/cellSize; k++){
						for(let j = 0; j < rockSize/cellSize; j++){
							if(oldCoords[0].x * cellSize === rocks[i].x + (j * cellSize) && oldCoords[0].y * cellSize == rocks[i].y + (k * cellSize)){
								collision = true;
								break; 		
							}
							if(fx === rocks[i].x + (j * cellSize) && fy === rocks[i].y + (k * cellSize)) setFoodCoords();	
						}
					}	
				}
			//CLONE OF THE MAIN ARRAY
			let coordsY = []; 
			let coordsX = [];

			for(let i = 0; i < oldCoords.length; i++){
				coordsY.push(oldCoords[i].y);
				coordsX.push(oldCoords[i].x);
			}

			for(let i = 1; i < snakeLength; i++){
				oldCoords[i].y = coordsY[i - 1];
				oldCoords[i].x = coordsX[i - 1];

				if(oldCoords[0].x === coordsX[i] && oldCoords[0].y === coordsY[i]) gameOver();
			}
			if(collision) gameOver();
				setDirection(dir);
			}
		}
	}
	function fillCell(x, y, style){
		if(style){
			ctx.drawImage(images[1], 0, 3*50, 50, 50, x, y, cellSize, cellSize);
		}
		else {
			ctx.drawImage(images[1], 3*50, 3*50, 50, 50, x, y, cellSize, cellSize);
		}
	}
}